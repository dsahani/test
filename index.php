<?php 
	function solution($string, $K) {
		$words = explode(" ", $string);
	    $msgs  = array();
	    $i 	   = 0;
	    while(count($words)>0) {
	    	$text = $words[0];
	    	if ( strlen($text) > $K) {
				return -1;
			}

			$msgs[$i] = $text;

			$text .= ' ' . $words[1];
			
			if ( strlen($text) <= $K) {
				$msgs[$i] = $text;
				array_shift($words);
				array_shift($words);
			} elseif (strlen($text) >= $K) {
				array_shift($words);
			}
			$i++;
	    }

		return count($msgs);
	}

	$string='SMS messages are really short';
	$limit = 12; 
	echo solution($string, $limit);
?>