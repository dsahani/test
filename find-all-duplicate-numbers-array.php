<?php 
	/* Print duplicate numbers */
	$arrNums = array(1, 1, 1, 2, 3, 4, 4, 5, 6, 6, 7, 8, 9, 9, 10, 11, 11);
	$arrTemp = [];

	for( $i=0; $i<count($arrNums); $i++ ) {
	  $num = $arrNums[$i];
	  if( $arrTemp[$num] >= 1 ){
	    $arrTemp[$num]++;
	  } else {
	    $arrTemp[$num] = 1;
	  }
	}

	echo "Duplicate numbers are: ";
	foreach ( $arrTemp as $key => $value ) {
		if( $value > 1 ) {
			echo $key . ' ';
		} 
	}
?>